+++
date = "2016-07-04T20:11:30+02:00"
draft = false
title = "Privacy"
type = "original"
+++

# Our Privacy Policy

We are committed to safeguarding the privacy of our website visitors; this policy sets out how we will treat your personal information.

**(1) What information do we collect?**

We may collect, store and use the following kinds of personal information:

- a) information about your computer and about your visits to and use of this website (including your IP address, geographical location, browser type and version, operating system, referral source, length of visit, page views, and website navigation);
- b) information that you provide to us for the purpose of subscribing to our website services, email notifications and/or newsletters;
- c) any other information that you choose to send to us;

**(2) Using your personal information**    

Personal information submitted to us via this website will be used for the purposes specified in this privacy policy or in relevant parts of the website.

We may use your personal information to:

- a) administer the website;
- b) send you general (non-marketing) commercial communications;     
- c) send you email notifications which you have specifically requested;
- d) send to you our newsletter and other marketing communications relating to our business which we think may be of interest to you by email or similar technology (you can inform us at any time if you no longer require marketing communications);

**(3) Disclosures**    

We may disclose information about you to [any of our employees, officers, agents, suppliers or subcontractors] insofar as reasonably necessary for the purposes as set out in this privacy policy.

In addition, we may disclose your personal information:

- a) to the extent that we are required to do so by law;    
- b) in connection with any legal proceedings or prospective legal proceedings;
- c) in order to establish, exercise or defend our legal rights

Except as provided in this privacy policy, we will not provide your information to third parties.

**(4) International data transfers**    

Information that we collect may be stored and processed in and transferred between any of the countries in which we operate in order to enable us to use the information in accordance with this privacy policy. Information which you provide may be transferred to countries which do not have data protection laws. You expressly agree to such transfers of personal information.

**(5) Security of your personal information**    

We will take reasonable technical and organisational precautions to prevent the loss, misuse or alteration of your personal information. Of course, data transmission over the internet is inherently insecure, and we cannot guarantee the security of data sent over the internet.

**(6) Policy amendments**    

We may update this privacy policy from time-to-time by posting a new version on our website. You should check this page occasionally to ensure you are happy with any changes.

**(7) Third party websites**    

The website contains links to other websites. We are not responsible for the privacy policies or practices of third party websites.

**(8) Updating information**    

Please let us know if the personal information which we hold about you needs to be corrected or updated.

**(9) Contact**

If you have any questions about this privacy policy or our treatment of your personal information, please write to us by email to {{< contact >}}.

+++
date = "2016-07-04T20:11:30+02:00"
draft = false
title = "Terms"
type = "original"
+++

# Our Disclaimer

**(1) Introduction**

This disclaimer governs your use of our website; by using our website, you accept this disclaimer in full. If you disagree with any part of this disclaimer, you must not use our website.

**(2) Intellectual property rights**

Unless otherwise stated, we or our licensors own the intellectual property rights in the website and material on the website. Subject to the licence below, all these intellectual property rights are reserved.

**(3) Licence to use website**

You may view, download for caching purposes only, and print pages or downloadable files from the website for your own personal use, subject to the restrictions below.

You must not:

- a) republish material from this website (including republication on another website);
- b) sell, rent or otherwise sub-license material from the website;
- c) show any material from the website in public;
- d) reproduce, duplicate, copy or otherwise exploit material on our website for a commercial purpose;
- e) edit or otherwise modify any material on the website; or
- f) redistribute material from this website except for content specifically and expressly made available for redistribution (such as our newsletter).

Where content is specifically made available for redistribution, it may only be redistributed within your business.

**(4) Limitation and exclusion of warranties and liability**

Whilst we endeavour to ensure that the information on this website is correct, we do not warrant its completeness or accuracy; nor do we commit to ensuring that the website remains available or that the material on the website is kept up-to-date.

To the maximum extent permitted by applicable law we exclude all representations, warranties and conditions relating to this website and the use of this website (including, without limitation, any warranties implied by law of satisfactory quality, fitness for purpose and/or the use of reasonable care and skill).

The limitations and exclusions of liability set out in this Section and elsewhere in this disclaimer govern all liabilities arising under the disclaimer or in relation to the subject matter of this disclaimer, including liabilities arising in contract, in tort (including negligence) and for breach of statutory duty.

We will not be liable to you in respect of any losses arising out of any event or events beyond our reasonable control.

We will not be liable to you in respect of any business losses, including (without limitation) loss of or damage to profits, income, revenue, use, production, anticipated savings, business, contracts, commercial opportunities or goodwill.

We will not be liable to you in respect of any loss or corruption of any data, database or software.

We will not be liable to you in respect of any special, indirect or consequential loss or damage.

**(5) Variation**

We may revise this disclaimer from time-to-time. The revised disclaimer will apply to the use of our website from the date of the publication of the revised disclaimer on our website. Please check this page regularly to ensure you are familiar with the current version.

**(6) Entire agreement**

This disclaimer, together with our privacy policy, constitutes the entire agreement between you and us in relation to your use of our website, and supersedes all previous agreements in respect of your use of this website.

**(7) Law and jurisdiction**

This disclaimer will be governed by and construed in accordance with {{< legal >}} law, and any disputes relating to this disclaimer will be subject to the exclusive jurisdiction of the courts of {{< legal >}}.

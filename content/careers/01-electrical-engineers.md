---
Title: Electrical Engineers
Description: We are looking for Engineers with experience of 3 - 5 years.
Location: South Africa
Date: 14.04.2016
Date: 2016-04-14T00:00:00+02:00
---

Lead EPC is looking for Electrical Engineers.

**Qualifications**
Btech or BSc. / BEng  ( National Diploma )

**Position Responsibilities:**
Work with own supervision.
Or work within Engineering team.
Interaction with clients at all levels.

Be able to deliver typical engineering deliverables:
- Single line diagrams
- Cable size calculations
- Fault level calculations
- MCC layouts
- Wiring diagrams

**Additional Requirements:**
Driver’s License Code 08

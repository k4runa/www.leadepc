---
title: Likusasa invests in Lead EPC
date: 2013-08-01T00:00:00+02:00
---

The management of Lead EPC is pleased to announce that the Likusasa group has acquired a significant shareholding in Lead Engineering and Projects (Pty) Ltd.

This is a significant show of confidence in the prospects for Lead EPC and it gives Lead EPC access to the widespread footprint of Likusasa across Africa and the Middle East. Commenting on this development, Hennie Engela, the Managing Director of Lead EPC, said

> “It is good to be part of the Likusasa team and to work once again with past colleagues who understand our business, and who share our vision and our passion for outstanding client service."

---
title: AMSA BOF fire damage repairs completed
date: 2013-05-09T00:00:00+02:00
---

Lead EPC has successfully completed the repairs at the BOF furnace at the Arcelor Mittal Vanderbijlpark works.

Working to an extremely demanding schedule and under hazardous conditions, the Lead EPC team repaired the extensive damage to the electrical and instrumentation installations of the basic oxygen furnace following a fire earlier this year. We are particularly proud that this project was completed without any safety incidents.

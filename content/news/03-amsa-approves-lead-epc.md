---
title: AMSA approves Lead EPC as a vendor
date: 2013-01-19T00:00:00+02:00
---

Following an audit of the quality, health, safety and environmental systems of Lead EPC, Arcelor Mittal approved Lead EPC as a vendor for engineering, procurement and construction services at its facilities throughout South Africa.

Commenting on the audit success, Hennie Engela the Managing Director of Lead EPC, stated that he was delighted at the show of confidence by Arcelor Mittal in the company.

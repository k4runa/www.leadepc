---
title: Sierra Rutile shutdown completed
date: 2013-05-09T00:00:00+02:00
---

Lead EPC has successfully completed a major shutdown at the D1 dredge and wet-plant at the Sierra Rutile plant in Sierra Leone. The shutdown was scheduled to take 4 weeks, and was completed on the 7th May, ahead of schedule.

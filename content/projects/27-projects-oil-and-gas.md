---
Title: Oil and Gas Projects
Year: 2015
---

#### SASOL POLYMERS
Implementation of 3x 525V Variable Speed Drives for Sasol Polymers at PP2 substation in Secunda.

The project include : Engineering, supply and installation of VSD's into new panels, installation on site and commissioning of equipment.

#### TARLTON TRANSNET TERMINAL
LeadEPC successfully executed another project with Siemens as Main Contractor for a fuel terminal of Transnet.

The scope of work included ;
-	Pre-manfacturing of piping
-	Site installation of piping and pipe supports
-	Installation of new valves & safety valves
-	Instrument calibration
-	Structure modification
-	WPS, PQR and Welder Qualifications to ASME standards
-	Material Procurement & Certification
-	Fabrication, including NDT testing (X-Rays)
-	Sandblasting & painting
-	Hydro pressure testing on site
-	Sub-Contractor management on site (x-rays), AIA and Hydro testing)

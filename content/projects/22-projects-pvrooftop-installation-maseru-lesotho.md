---
Title: PV Rooftop Installation, Maseru, Lesotho
Year: 2015
---

Location: Maseru, Lesotho

Duration / Finish: 	1 Months / February - May 2015

Description: 	Electrical & Instrumentation Installation/Construction & Commissioning Assistance

Scope of Work:
	- Construction/Installation of electrical equipment including
	- PV roof top system
	- Construction/Installation of Electrical Equipment
	- Plant commissioning assistance
	- Installation Quality control
	- Site health, safety and environmental compliance

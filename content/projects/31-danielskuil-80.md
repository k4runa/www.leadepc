---
Title: PV Power Generation for 80 Rural Houses in Danielskuil North Cape
Year: 2016
---

Detail Engineering, Procurement, Installation and Commissioning of small scale PV installations with battery backup for 80 rural houses in the Northern Cape.

---
Title: PV Rooftop Installation, Postmasburg, Northern Cape
Year: 2015
---

Engineering, Procurement, Construction and Commissioning of a 20kWp rooftop PV grid tied installation for Jasper Operations and Maintenance Building (75MWp PV Park) near Postmasburg in the Northern Cape, complete with weather and monitoring station.

Yingli 295Wp panels are used with ABB TRIO 20kW Inverter.

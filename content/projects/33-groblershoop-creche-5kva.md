---
Title: PV Power Generation for Groblershoop Creche North Cape
Year: 2016
---

Detail Engineering, Procurement, Installation and Commissioning of small scale PV installations with battery backup for Groblershoop Creche in Northern Cape.

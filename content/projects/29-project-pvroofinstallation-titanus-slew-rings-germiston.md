---
Title: PV Rooftop Installation, Titanus Slew Rings, Germiston
Year: 2016
---

Engineering, Procurement, Construction and Commissioning of a 275kWp rooftop PV grid tied installation for TSR Slew Rings in Germiston, complete with weather station and Generator Control.
Trina Solar 310Wp panels are used with SMA 25kW Inverters.

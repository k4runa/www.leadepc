/* ============================================================
 * Plugin Core Init
 * For DEMO purposes only. Extract what you need.
 * ============================================================ */
$(document).ready(function() {
    'use strict';

    // Initialize Slider
    var contentSlider = new Swiper('#inner-content-slider', {
        paginationClickable: true,
        speed:1000,
        effect: 'fade',
        fade: {
            crossFade: true
        },
        autoplay:2000
    });

});